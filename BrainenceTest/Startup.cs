using System;
using System.Text.Json.Serialization;
using BrainenceTest.Middlewares;
using BrainenceTest.Providers;
using BrainenceTest.Repositories;
using BrainenceTest.Services;
using BrainenceTest.Settings;
using BrainenceTest.Workflow;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace BrainenceTest
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            var memoryCacheKey = Configuration.GetSection("MemoryCacheSettings").Get<MemoryCacheSettings>();
            services.AddSingleton(_ => memoryCacheKey);
            
            services.AddMvc().AddJsonOptions(opts =>
            {
                opts.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            });
            services.AddTransient<IExchangeRateProvider, ExchangeRateProvider>();
            services.AddTransient<ICurrencyExchangeWorkflow, CurrencyExchangeWorkflow>();
            services.AddTransient<IExchangeOrdersService, ExchangeOrdersService>();
            services.AddTransient<IExchangeOrdersRepository, ExchangeOrdersRepository>();
            
            services.AddSpaStaticFiles(configuration => { configuration.RootPath = "ClientApp/build"; });
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMiddleware<RequestLoggingMiddleware>();
            
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}