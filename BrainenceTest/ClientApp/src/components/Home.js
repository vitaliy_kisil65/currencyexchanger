import React, {Component} from 'react';
import './Home.css';

export class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);
        this.state = {
            convertedValue: "",
            amount: "",
            fromCurrency: "USD",
            toCurrency: "USD"
        };
        this.handleInputTo = this.handleInputTo.bind(this);
        this.handleInputFrom = this.handleInputFrom.bind(this);
        this.handleToCurrency = this.handleToCurrency.bind(this);
        this.handleFromCurrency = this.handleFromCurrency.bind(this);
    }

    async onPressInput() {
        if (this.state.amount === "" || this.state.amount === null){
            const response = await fetch(`api/currencyExchange/getExchangeRate?amountOfMoney=${this.state.convertedValue}&from=${this.state.toCurrency}&to=${this.state.fromCurrency}`);
            const data = await response.json();
            this.setState({amount: JSON.stringify(data)});
        }
        else {
            const response = await fetch(`api/currencyExchange/getExchangeRate?amountOfMoney=${this.state.amount}&from=${this.state.fromCurrency}&to=${this.state.toCurrency}`);
            const data = await response.json();
            this.setState({convertedValue: JSON.stringify(data)});
        }
    };

    handleFromCurrency(event) {
        this.setState({fromCurrency: event.target.value});
    }

    handleToCurrency(event) {
        this.setState({toCurrency: event.target.value});
    }

    handleInputFrom(event) {
        this.setState({amount: event.target.value})
        this.setState({convertedValue: ""})
    }
    handleInputTo(event) {
        this.setState({convertedValue: event.target.value})
        this.setState({amount: ""})
    }
    

    render() {
        return (
            <div className="converter_body">
                <div className="converter_item">
                    <span>Amount</span>

                    <div className="converter_item_value">
                        <input type="number" value={this.state.amount} onChange={this.handleInputFrom}/>
                        <select value={this.state.fromCurrency} onChange={this.handleFromCurrency}>
                            <option value="USD">USD</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                            <option value="CHF">CHF</option>
                        </select>
                    </div>
                </div>

                <div className="converter_item">
                    <span>Converted to</span>

                    <div className="converter_item_value">
                        <input type="number" value={this.state.convertedValue} onChange={this.handleInputTo}/>
                        <select value={this.state.toCurrency} onChange={this.handleToCurrency}>
                            <option value="USD">USD</option>
                            <option value="EUR">EUR</option>
                            <option value="GBP">GBP</option>
                            <option value="CHF">CHF</option>
                        </select>
                    </div>
                    <div>
                        <button type="submit" onClick={() =>
                            this.onPressInput()
                        }>Exchenge
                        </button>
                    </div>

                </div>
            </div>
        );

    }
}
