import React, { Component } from 'react';

export class ExchangeHistory extends Component {
  static displayName = ExchangeHistory.name;

  constructor(props) {
    super(props);
    this.state = { exchangeOrders: [], loading: true };
  }

  componentDidMount() {
    this.populateExchangeHistory();
  }

  static renderExchangeOrdersTable(exchangeOrders) {
    return (
      <table className='table table-striped' aria-labelledby="tabelLabel">
        <thead>
          <tr>
            <th>Date</th>
            <th>From currency</th>
            <th>From amount</th>
            <th>Exchange rate</th>
            <th>To currency</th>
            <th>To amount</th>
          </tr>
        </thead>
        <tbody>
          {exchangeOrders.map(exchangeOrder =>
            <tr key={exchangeOrder.id}>
              <td>{exchangeOrder.creationDateUtc}</td>
              <td>{exchangeOrder.from.currency}</td>
              <td>{exchangeOrder.from.amount}</td>
              <td>{exchangeOrder.rate}</td>
              <td>{exchangeOrder.to.currency}</td>
              <td>{exchangeOrder.to.amount}</td>
            </tr>
          )}
        </tbody>
      </table>
    );
  }

  render() {
    let contents = this.state.loading
      ? <p><em>Loading...</em></p>
      : ExchangeHistory.renderExchangeOrdersTable(this.state.exchangeOrders);

    return (
      <div>
        <h1 id="tabelLabel" >Exchange history</h1>
        {contents}
      </div>
    );
  }

  async populateExchangeHistory() {
    const response = await fetch('api/currencyExchange/exchangeHistory');
    const data = await response.json();
    this.setState({ exchangeOrders: data, loading: false });
  }
}
