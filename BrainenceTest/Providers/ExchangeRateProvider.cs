﻿using System;
using System.Globalization;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BrainenceTest.Models;
using Microsoft.Extensions.Logging;

namespace BrainenceTest.Providers
{
    public class ExchangeRateProvider : IExchangeRateProvider
    {
        private readonly ILogger<ExchangeRateProvider> _logger;

        private const string Pattern = "[0-9]{0,10}[.][0-9]{0,10}";

        public ExchangeRateProvider(ILogger<ExchangeRateProvider> logger)
        {
            _logger = logger;
        }

        public Task<decimal> ProvideAsync(Currency from, Currency to)
        {
            if (from == to)
            {
                return Task.FromResult(1m);
            }
            
            return GetExchangeRateAsync(from, to);
        }

        private async Task<decimal> GetExchangeRateAsync(Currency from, Currency to)
        {
            using var client = new HttpClient();
            
            var result = await client.GetStringAsync($"https://api.exchangeratesapi.io/latest?base={from}&symbols={to}");
            var match = ParsResponse(result);
            
            var coefficient = Convert.ToDecimal(match.Value.Replace(",", "."), CultureInfo.InvariantCulture);
            return coefficient;
        }

        private Match ParsResponse(string result)
        {
            if (string.IsNullOrEmpty(result))
            {
                _logger.LogError("Result is null");
                return null;
            }

            return Regex.Match(result, Pattern);
        }
    }
}