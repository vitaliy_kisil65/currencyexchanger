﻿using System.Threading.Tasks;
using BrainenceTest.Models;

namespace BrainenceTest.Providers
{
    public interface IExchangeRateProvider
    {
        Task<decimal> ProvideAsync(Currency from, Currency to);
    }
}