﻿using System.Threading.Tasks;
using BrainenceTest.Models;

namespace BrainenceTest.Workflow
{
    public interface ICurrencyExchangeWorkflow
    {
        Task<decimal> ExchangeAsync(decimal amountOfMoney, Currency from, Currency to);
    }
}