﻿using System;
using System.Threading.Tasks;
using BrainenceTest.Models;
using BrainenceTest.Providers;
using BrainenceTest.Services;

namespace BrainenceTest.Workflow
{
    public class CurrencyExchangeWorkflow : ICurrencyExchangeWorkflow
    {
        private readonly IExchangeRateProvider _exchangeRateProvider;
        private readonly IExchangeOrdersService _exchangeOrdersService;

        public CurrencyExchangeWorkflow(IExchangeRateProvider exchangeRateProvider, IExchangeOrdersService exchangeOrdersService)
        {
            _exchangeRateProvider = exchangeRateProvider;
            _exchangeOrdersService = exchangeOrdersService;
        }

        public async Task<decimal> ExchangeAsync(decimal amountOfMoney, Currency from, Currency to)
        {
            var exchangeRate = await _exchangeRateProvider.ProvideAsync(from, to);

            var appliedMoneyAmount = ApplyExchangeRate(amountOfMoney, exchangeRate);

            var exchangeOrder = new ExchangeOrder
            {
                From = new ExchangeDetails
                {
                    Amount = amountOfMoney,
                    Currency = @from
                },
                To=new ExchangeDetails
                {
                    Amount = appliedMoneyAmount,
                    Currency = to
                },
                Rate = exchangeRate,
                CreationDateUtc = DateTime.UtcNow
            };
            
            _exchangeOrdersService.AddExchangeOrder(exchangeOrder);

            return appliedMoneyAmount;
        }

        private static decimal ApplyExchangeRate(decimal moneyAmount, decimal exchangeRate)
        {
            var result = moneyAmount * exchangeRate;
            result = Math.Truncate(result * 100) / 100m;
            return result;
        }
    }
}