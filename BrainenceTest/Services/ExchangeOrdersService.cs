﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrainenceTest.Models;
using BrainenceTest.Repositories;

namespace BrainenceTest.Services
{
    public class ExchangeOrdersService : IExchangeOrdersService
    {
        private readonly IExchangeOrdersRepository _exchangeOrdersRepository;

        public ExchangeOrdersService(IExchangeOrdersRepository exchangeOrdersRepository)
        {
            _exchangeOrdersRepository = exchangeOrdersRepository;
        }

        public void AddExchangeOrder(ExchangeOrder order)
        {
            order.Id=Guid.NewGuid();
            
            var exchangeOrders = _exchangeOrdersRepository.GetExchangeOrders();
            _exchangeOrdersRepository.AddExchangeOrder(exchangeOrders, order);
        }

        public List<ExchangeOrder> GetExchangeOrders()
        {
            var exchangeOrders = _exchangeOrdersRepository.GetExchangeOrders();
            return exchangeOrders.OrderBy(e => e.CreationDateUtc).ToList();
        }
    }
}