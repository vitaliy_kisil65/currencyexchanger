﻿using System.Collections.Generic;
using BrainenceTest.Models;

namespace BrainenceTest.Services
{
    public interface IExchangeOrdersService
    {
        void AddExchangeOrder(ExchangeOrder order);
        List<ExchangeOrder> GetExchangeOrders();
    }
}