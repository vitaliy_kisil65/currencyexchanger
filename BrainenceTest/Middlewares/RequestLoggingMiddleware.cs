﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Logging;

namespace BrainenceTest.Middlewares
{
    public class RequestLoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public RequestLoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<RequestLoggingMiddleware>();
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var query = context.Request.GetDisplayUrl();
            _logger.LogDebug($"Request started: {query}");
            
            var watch = new Stopwatch();
            
            watch.Start();

            try
            {
                await _next(context);
            }
            finally
            {
                watch.Stop();
                _logger.LogInformation($"Request: {query} stopped. Execution time: {watch.Elapsed}");
            }
        }
    }
}