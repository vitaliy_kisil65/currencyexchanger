﻿namespace BrainenceTest.Models
{
    public enum Currency
    {
        USD,
        EUR, 
        GBP,
        CHF
    }
}