﻿using System;

namespace BrainenceTest.Models
{
    public class ExchangeOrder
    {
        public Guid Id { get; set; }
       
        public ExchangeDetails From { get; set; }
        
        public ExchangeDetails To { get; set; }
        
        public decimal Rate { get; set; }
        
        public DateTime CreationDateUtc { get; set; }
    }
}