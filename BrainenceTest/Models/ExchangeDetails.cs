﻿
namespace BrainenceTest.Models
{
    public class ExchangeDetails
    {
        public Currency Currency { get; set; }
        public decimal Amount { get; set; }
    }
}