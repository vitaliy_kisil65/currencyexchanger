﻿using System;

namespace BrainenceTest.Settings
{
    public sealed class MemoryCacheSettings
    {
        public Guid Key { get; set; }
    }
}