﻿using System.Collections.Generic;
using BrainenceTest.Models;

namespace BrainenceTest.Repositories
{
    public interface IExchangeOrdersRepository
    {
        void AddExchangeOrder(List<ExchangeOrder> exchangeOrders, ExchangeOrder exchangeOrder);
        List<ExchangeOrder> GetExchangeOrders();
    }
}