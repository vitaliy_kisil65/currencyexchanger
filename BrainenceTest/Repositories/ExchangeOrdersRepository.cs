﻿using System.Collections.Generic;
using BrainenceTest.Models;
using BrainenceTest.Settings;
using Microsoft.Extensions.Caching.Memory;

namespace BrainenceTest.Repositories
{
    public class ExchangeOrdersRepository : IExchangeOrdersRepository
    {
        private readonly IMemoryCache _memoryCache;
        private readonly MemoryCacheSettings _memoryCacheSettings;

        public ExchangeOrdersRepository(
            IMemoryCache memoryCache, 
            MemoryCacheSettings memoryCacheSettings)
        {
            _memoryCache = memoryCache;
            _memoryCacheSettings = memoryCacheSettings;
        }

        public void AddExchangeOrder(List<ExchangeOrder> exchangeOrders, ExchangeOrder exchangeOrder)
        {
            exchangeOrders.Add(exchangeOrder);
            
            _memoryCache.Set(_memoryCacheSettings.Key, exchangeOrders, new MemoryCacheEntryOptions());
        }
        
        public List<ExchangeOrder> GetExchangeOrders()
        {
            if (!_memoryCache.TryGetValue(_memoryCacheSettings.Key, out List<ExchangeOrder> exchangeOrders))
            {
                return new List<ExchangeOrder>();
            }
            
            return exchangeOrders;
        }
    }
}