﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using BrainenceTest.Models;
using BrainenceTest.Services;
using BrainenceTest.Workflow;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace BrainenceTest.Controllers
{
    [Route("api/currencyExchange")]
    [ApiController]
    public class CurrencyExchangeController : ControllerBase
    {
        private readonly ICurrencyExchangeWorkflow _currencyExchangeWorkflow;
        private readonly IExchangeOrdersService _exchangeOrdersService;

        public CurrencyExchangeController(
            IExchangeOrdersService exchangeOrdersService,
            ICurrencyExchangeWorkflow currencyExchangeWorkflow)
        {
            _exchangeOrdersService = exchangeOrdersService;
            _currencyExchangeWorkflow = currencyExchangeWorkflow;
        }

        [HttpGet]
        [Route("getExchangeRate")]
        public async Task<decimal> GetExchangeRateAsync(
            [Required] [Range(0, double.MaxValue)] decimal amountOfMoney,
            [Required] Currency from,
            [Required] Currency to)
        {
            var value = await _currencyExchangeWorkflow.ExchangeAsync(amountOfMoney, from, to);
            return value;
        }

        [HttpGet]
        [Route("exchangeHistory")]
        public List<ExchangeOrder> GetAllExchanges()
        {
            return _exchangeOrdersService.GetExchangeOrders();
        }
    }
}